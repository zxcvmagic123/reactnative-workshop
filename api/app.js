import React, { Component } from 'react';
import { Text, View,FlatList,StyleSheet,Image,SafeAreaView ,TouchableOpacity,ScrollView } from 'react-native';
import Constants from 'expo-constants';

let time = new Date().toLocaleString();
let TodayUrl = 'https://covid19.th-stat.com/api/open/today';
let TCaseDetailUrl = 'https://covid19.th-stat.com/api/open/cases';



export default class app extends Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true,
      dataToday:{Confirmed: 0, Recovered: 0, Hospitalized:0, Deaths: 0}
      ,dataCase:{
        Age: 17,
        ConfirmDate: "2020-05-15 00:00:00",
        Detail: null,
        District: "บางละมุง",
        Gender: "ชาย",
        GenderEn: "Male",
        Nation: "ไทย",
        NationEn: "Thai",
        No: "3025",
        Province: "ชลบุรี",
        ProvinceEn: "Chonburi",
        ProvinceId: 9,
        StatQuarantine: 1
      }}
    } 
    async loadDataCase() {
      try {
        let res = await fetch("https://covid19.th-stat.com/api/open/cases");
        let respJSON = await res.json();
        
        console.log(respJSON.Data[0]);
        console.log("---- finish load -----")
        this.setState({ dataCase: respJSON.Data });
        } catch (err) {
        console.log(err)
        }
    }
    async componentDidMount(){
      // let CovidToday = await this.getCovidApi();
      //   this.setState({
      //     dataToday: CovidToday
      //   })
        console.log("---- did mount -----");
        this.loadData();
        // let CovidCase = await this.getCaseApi();
        // this.setState({
        //   dataCase: CovidCase
        // }) 
      }

   getCovidApi = async() =>{
     try{
       let response = await fetch(TodayUrl);
       let responseJson = await response.json();
        return responseJson;
     } catch(error){
       console.error(error);
       
     }
   }
   getCaseApi = async() =>{
    try{
      let res = await fetch(TCaseDetailUrl);
      let respJSON = await res.json()
      // console.log(respJSON)
      // console.log(respJSON.Data[0])
      // console.log(respJSON.Data[0].ConfirmDate)
      return respJSON;
      }catch(err){
      console.log(err)
      }
  }
  // try{
  //   let res = await fetch("https://covid19.th-stat.com/api/open/cases");
  //   let respJSON = await res.json()
  //   console.log(respJSON)
  //   console.log(respJSON.Data[0])
  //   console.log(respJSON.Data[0].ConfirmDate)
  //   }catch(err){
  //   console.log(err)
  //   }
  // getConfirmedDateCase(input) {
  //   console.log(this.state.dataCase.Data);

  //   if(this.state.dataCase.Data.Key ==input){
  //     this.setState({result:this.state.dataCase.Data})
  //   }
  // }
    
  // fetchData(text) {
  //   this.setState({ text });
  //   const apikey = '&apikey=thewdb';
  //   const url = 'http://www.omdbapi.com/?s=';
  //   fetch(url + text + url)
  //     .then(response => response.json())
  //     .then((responseJson) => {
  //       this.setState({
  //         dataSource: responseJson.Search,
  //       });
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // }
  render() {
    // console.log(this.state.dataToday);
    // console.log(this.state.dataCase.Data[0]);
    // console.log(this.state.dataCase.Data[0].ConfirmDate);
    console.log("---- main render -----")
    let obj = this.state.dataCase[0];
    //console.log(obj.Age);
    return(

     <>
           {/* <Text>{time}</Text> */}
           {/* <FlatList
          data={this.getConfirmedDateCase(3025)}
          renderItem={({item}) => 
          <View style={styles.list}>
            
            <ScrollView>       
    <Text style={styles.item}>.{item.ConfirmDate}</Text>
            </ScrollView>
         
          </View>
          } */}
        />
      {/* <FlatList
          data={this.state.dataCase.Data}
          renderItem={({item}) => 
          <View style={styles.list}>
            
            <ScrollView>       
    <Text style={styles.item}>.{item.ConfirmDate}</Text>
            </ScrollView>
         
          </View>
          }
        /> */}
     
    </>
    )
  }
}  

const filterResultsByYield = (serv) => {
  return this.dataCase.filter((result) => {
    return this.dataCase.Data.Age > serv;
  });
};

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 12,
    fontWeight:'bold',
    height: 44,
    color:'white'
  },
  list:{
    flexDirection:'row',
    padding:10,
    alignItems:'center',
    borderBottomColor:'grey',
    borderBottomWidth:2,
    backgroundColor:'#73BAF5'
  },
  Image: {
    width:150,
    height:150,
    alignContent:'center'
  }
});
